from django.shortcuts import render
def home(request):
    return render(request, 'homeApp/home.html', {'text': 'Abdur Rahman','number':123})
def loading(request):
    return render(request, 'homeApp/loading.html', {'title': 'loading'})
def signIn(request):
    return render(request, 'homeApp/signIn.html', {'title': 'signIn'})