from django.db import models

# Create your models here.
UTILITY_CHOICES = (
    ('choise1', "choise1"),
    ('choise2', "choise2"),
    ('choise3', "choise3")
)


class wasa_user(models.Model):
    user_name = models.CharField(max_length=100, null=False, blank=False)
    password = models.CharField(max_length=100, null=False, blank=False)
    email = models.CharField(max_length=100, null=False, blank=False, unique=True)
    zone_id = models.CharField(max_length=100, null=False, blank=False)
    user_role = models.ForeignKey(max_length=100, null=False, blank=False)
    delete = models.BooleanField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(max_length=10, null=False, blank=False)
    updated_by = models.IntegerField(max_length=10)


class zone(models.Model):
    zone_name = models.CharField(max_length=100, null=False, blank=False)
    location = models.CharField(max_length=100, null=False, blank=False)
    delete = models.BooleanField(null=False, blank=False)


class utility(models.Model):
    utility_name = models.CharField(max_length=100, null=False, blank=False)
    zone_id = models.ForeignKey(null=False, blank=False)
    account_number = models.IntegerField(null=False, blank=False)
    meter_number = models.IntegerField(null=False, blank=False)
    consumer_number = models.IntegerField(null=False, blank=False)
    utility_house_name = models.CharField(max_length=100, null=False, blank=False)
    delete=models.BooleanField(null=False, blank=False)

class bill(models.Model):
    date = models.DateTimeField(null=False, blank=False)
    bundle_no = models.ForeignKey( null=False, blank=False, primary_key=True)
    zone_id = models.ForeignKey( null=False, blank=False)
    utility_name = models.CharField(max_length=100, null=False, blank=False)
    pf_charge = models.FloatField(max_length=100, null=False, blank=False)
    govt_duty = models.FloatField( null=False, blank=False)
    bill_amount = models.FloatField(max_length=100, null=False, blank=False)
    delete = models.BooleanField(null=False, blank=False)

class status(models.Model):
    date = models.DateTimeField(null=False, blank=False)
    bundle_no = models.IntegerField(null=False, blank=False)
    zone_id = models.IntegerField(null=False, blank=False)
    zone_status = models.CharField(max_length=100, null=False, blank=False)
    ac_status = models.CharField(max_length=100, null=False, blank=False)
    delete = models.BooleanField(null=False, blank=False)

class app_log(models.Model):
    date = models.DateTimeField(null=False, blank=False)
    bundle_no = models.IntegerField( null=False, blank=False)
    user_id = models.CharField(max_length=100, null=False, blank=False)
    operation = models.CharField(max_length=100, null=False, blank=False)
    delete = models.BooleanField(null=False, blank=False)