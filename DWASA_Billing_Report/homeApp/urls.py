
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
     path('', views.home, name="home"),
     path('loading', views.loading, name="loading"),
     path('signIn', views.signIn, name="signIn"),

]
