from django.contrib import admin

# Register your models here.
from .models import wasa_user
admin.site.register(wasa_user)

from .models import zone
admin.site.register(zone)

from .models import utility
admin.site.register(utility)
